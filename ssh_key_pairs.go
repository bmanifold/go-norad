package norad

import (
	"fmt"
)

type SshKeyPairsService struct {
	client *Client
}

type SshKeyPair struct {
	Id             int    `json:"id"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	KeySignature   string `json:"key_signature"`
	OrganizationId int    `json:"organization_id"`
}

type CreateSshKeyPairOptions struct {
	SshKeyPairCreationParameters `json:"ssh_key_pair"`
}

type UpdateSshKeyPairOptions struct {
	SshKeyPairUpdateParameters `json:"ssh_key_pair"`
}

type SshKeyPairCreationParameters struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Username    string `json:"username"`
	Key         string `json:"key"`
}

type SshKeyPairUpdateParameters struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (kp SshKeyPair) String() string {
	return Stringify(kp)
}

func (s *SshKeyPairsService) ListSshKeyPairs(oid interface{}) ([]*SshKeyPair, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/ssh_key_pairs", org_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*SshKeyPair
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshKeyPairsService) GetSshKeyPair(id interface{}) (*SshKeyPair, *Response, error) {
	kp_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("ssh_key_pairs/%s", kp_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *SshKeyPair
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshKeyPairsService) CreateSshKeyPair(oid interface{}, opt *CreateSshKeyPairOptions) (*SshKeyPair, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/ssh_key_pairs", org_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SshKeyPair)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshKeyPairsService) UpdateSshKeyPair(id interface{}, opt *UpdateSshKeyPairOptions) (*SshKeyPair, *Response, error) {
	kp_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("ssh_key_pairs/%s", kp_id)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SshKeyPair)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshKeyPairsService) DeleteSshKeyPair(id interface{}) (*Response, error) {
	kp_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("ssh_key_pairs/%s", kp_id)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
