package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestSecurityTestServiceParseError(t *testing.T) {
	service := &SecurityTestsService{}
	_, _, err := service.List(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &SecurityTestsService{}
	_, _, err = service.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &SecurityTestsService{}
	_, _, err = service.Create(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &SecurityTestsService{}
	_, _, err = service.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &SecurityTestsService{}
	_, err = service.Delete(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestSecurityTestsServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing SecurityTests with expected error %s", c.expectedError), func(t *testing.T) {
			filters := SecurityTestFilters{TestName: "foo"}
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"security_test_repositories/1/security_containers",
				&filters,
			).Return(&http.Request{}, c.requestError)
			security_tests := make([]*SecurityTest, 0)
			client.On(
				"Do",
				&http.Request{},
				&security_tests,
			).Return(&Response{}, c.doError)

			service := &SecurityTestsService{client: &client}
			_, _, err := service.List(1, &filters)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestSecurityTestsServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a SecurityTest with expected error %s", c.expectedError), func(t *testing.T) {
			opts := SecurityTestOptions{Parameters: SecurityTestParameters{Configurable: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"security_test_repositories/1/security_containers",
				&opts,
			).Return(&http.Request{}, c.requestError)
			security_test := new(SecurityTest)
			client.On(
				"Do",
				&http.Request{},
				security_test,
			).Return(&Response{}, c.doError)

			service := &SecurityTestsService{client: &client}
			_, _, err := service.Create(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestSecurityTestsServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a SecurityTest with expected error %s", c.expectedError), func(t *testing.T) {
			opts := SecurityTestOptions{Parameters: SecurityTestParameters{Configurable: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"security_containers/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			security_test := new(SecurityTest)
			client.On(
				"Do",
				&http.Request{},
				security_test,
			).Return(&Response{}, c.doError)

			service := &SecurityTestsService{client: &client}
			_, _, err := service.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestSecurityTestsServiceGet(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a SecurityTest with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"security_containers/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			security_test_obj := new(SecurityTest)
			client.On(
				"Do",
				&http.Request{},
				security_test_obj,
			).Return(&Response{}, c.doError)

			service := &SecurityTestsService{client: &client}
			_, _, err := service.Get(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestSecurityTestsServiceDelete(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a SecurityTest with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"DELETE",
				"security_containers/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			client.On(
				"Do",
				&http.Request{},
				nil,
			).Return(&Response{}, c.doError)

			service := &SecurityTestsService{client: &client}
			_, err := service.Delete(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
