package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestEnforcementServiceParseError(t *testing.T) {
	service := &EnforcementsService{}
	_, _, err := service.List(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &EnforcementsService{}
	_, _, err = service.Create(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &EnforcementsService{}
	_, err = service.Delete(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestEnforcementsServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing Enforcements with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"organizations/1/enforcements",
				nil,
			).Return(&http.Request{}, c.requestError)
			enforcements := make([]*Enforcement, 0)
			client.On(
				"Do",
				&http.Request{},
				&enforcements,
			).Return(&Response{}, c.doError)

			service := &EnforcementsService{client: &client}
			_, _, err := service.List(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestEnforcementsServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a Enforcement with expected error %s", c.expectedError), func(t *testing.T) {
			opts := EnforcementOptions{Parameters: EnforcementParameters{RequirementGroupId: 1}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"organizations/1/enforcements",
				&opts,
			).Return(&http.Request{}, c.requestError)
			enforcement := new(Enforcement)
			client.On(
				"Do",
				&http.Request{},
				enforcement,
			).Return(&Response{}, c.doError)

			service := &EnforcementsService{client: &client}
			_, _, err := service.Create(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestEnforcementsServiceDelete(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a Enforcement with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"DELETE",
				"enforcements/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			client.On(
				"Do",
				&http.Request{},
				nil,
			).Return(&Response{}, c.doError)

			service := &EnforcementsService{client: &client}
			_, err := service.Delete(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
