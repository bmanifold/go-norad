package norad

import (
	"fmt"
)

type OrganizationsService struct {
	client *Client
}

type Organization struct {
	Id            int    `json:"id"`
	Uid           string `json:"uid"`
	Slug          string `json:"slug"`
	Token         string `json:"token"`
	Configuration *OrganizationConfiguration
}

type CreateOrganizationOptions struct {
	OrganizationParameters `json:"organization"`
}

type UpdateOrganizationOptions struct {
	OrganizationParameters `json:"organization"`
}

type OrganizationParameters struct {
	Uid string `json:"uid"`
}

func (o Organization) String() string {
	return Stringify(o)
}

func (s *OrganizationsService) ListOrganizations() ([]*Organization, *Response, error) {
	req, err := s.client.NewRequest("GET", "organizations", nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*Organization
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *OrganizationsService) GetOrganization(id interface{}) (*Organization, *Response, error) {
	oid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s", oid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *Organization
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *OrganizationsService) CreateOrganization(opt *CreateOrganizationOptions) (*Organization, *Response, error) {
	req, err := s.client.NewRequest("POST", "organizations", opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(Organization)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *OrganizationsService) UpdateOrganization(id interface{}, opt *UpdateOrganizationOptions) (*Organization, *Response, error) {
	oid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s", oid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(Organization)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *OrganizationsService) DeleteOrganization(id interface{}) (*Response, error) {
	oid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("organizations/%s", oid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
