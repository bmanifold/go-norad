package norad

import (
	"fmt"
)

type RelaysService struct {
	client NoradClient
}

type Relay struct {
	Id             int    `json:"id"`
	OrganizationId int    `json:"organization_id"`
	PublicKey      string `json:"public_key"`
	QueueName      string `json:"queue_name"`
	LastHeartbeat  string `json:"last_heartbeat"`
	Fingerprint    string `json:"key_signature"`
	State          string
	Verified       bool
}

type RelayOptions struct {
	Parameters RelayParameters `json:"docker_relay"`
}

type RelayParameters struct {
	Verified bool `json:"verified"`
}

func (r Relay) String() string {
	return Stringify(r)
}

func (service *RelaysService) List(oid interface{}) ([]*Relay, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/docker_relays", org_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	relays := make([]*Relay, 0)
	resp, err := service.client.Do(req, &relays)
	if err != nil {
		return nil, resp, err
	}

	return relays, resp, err
}

func (service *RelaysService) Get(id interface{}) (*Relay, *Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("docker_relays/%s", rid)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	relay := new(Relay)
	resp, err := service.client.Do(req, relay)
	if err != nil {
		return nil, resp, err
	}

	return relay, resp, err
}

func (service *RelaysService) Update(id interface{}, opt *RelayOptions) (*Relay, *Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("docker_relays/%s", rid)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	relay := new(Relay)
	resp, err := service.client.Do(req, relay)
	if err != nil {
		return nil, resp, err
	}

	return relay, resp, err
}

func (service *RelaysService) Delete(id interface{}) (*Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("docker_relays/%s", rid)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}
