package norad

import (
	"fmt"
	"strconv"
)

// Utility function to make it easy to reference things by ints or strings
// Inspired by the go-gitlab library
func parseID(id interface{}) (string, error) {
	switch v := id.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("invalid ID type %#v, the ID must be an int or a string", id)
	}
}
