package norad

import (
	"net/http"
	"reflect"
	"regexp"
	"testing"

	"github.com/stretchr/testify/mock"
)

type MockClient struct {
	mock.Mock
}

func (m *MockClient) NewRequest(method string, path string, opt interface{}) (*http.Request, error) {
	ret := m.Called(method, path, opt)
	return ret.Get(0).(*http.Request), ret.Error(1)

}

func (m *MockClient) Do(req *http.Request, v interface{}) (*Response, error) {
	ret := m.Called(req, v)
	return ret.Get(0).(*Response), ret.Error(1)
}

func TestNewClient(t *testing.T) {
	http_client := new(http.Client)
	client := NewClient(http_client, "norad.cisco.com", "12345", true)
	v := reflect.ValueOf(*client)
	struct_type := reflect.TypeOf(*client)

	for i := 0; i < v.NumField(); i++ {
		if match, _ := regexp.Match("^[a-z]", []byte(struct_type.Field(i).Name)); match {
			continue
		}
		if v.Field(i).IsNil() {
			t.Errorf("Service not initialized: %s", struct_type.Field(i).Name)
		}
	}
}
