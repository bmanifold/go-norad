package norad

import (
	"fmt"
)

type WebApplicationConfigsService struct {
	client *Client
}

type WebApplicationConfig struct {
	Id                         int    `json:"id"`
	AuthType                   string `json:"auth_type"`
	UrlBlacklist               string `json:"url_blacklist"`
	StartingPagePath           string `json:"starting_page_path"`
	LoginFormUsernameFieldName string `json:"login_form_username_field_name"`
	LoginFormPasswordFieldName string `json:"login_form_password_field_name"`
	ServiceId                  int    `json:"service_id"`
}

type WebApplicationConfigOptions struct {
	WebApplicationConfigParameters `json:"web_application_config"`
}

type WebApplicationConfigParameters struct {
	AuthType                   string `json:"auth_type"`
	UrlBlacklist               string `json:"url_blacklist"`
	StartingPagePath           string `json:"starting_page_path"`
	LoginFormUsernameFieldName string `json:"login_form_username_field_name"`
	LoginFormPasswordFieldName string `json:"login_form_password_field_name"`
}

func (w WebApplicationConfig) String() string {
	return Stringify(w)
}

func (s *WebApplicationConfigsService) GetWebApplicationConfig(id interface{}) (*WebApplicationConfig, *Response, error) {
	wid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("web_application_configs/%s", wid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *WebApplicationConfig
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationConfigsService) CreateWebApplicationConfig(sid interface{}, opt *WebApplicationConfigOptions) (*WebApplicationConfig, *Response, error) {
	service_id, err := parseID(sid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s/web_application_configs", service_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(WebApplicationConfig)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationConfigsService) UpdateWebApplicationConfig(id interface{}, opt *WebApplicationConfigOptions) (*WebApplicationConfig, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("web_application_configs/%s", sid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(WebApplicationConfig)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationConfigsService) DeleteWebApplicationConfig(id interface{}) (*Response, error) {
	wid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("web_application_configs/%s", wid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
