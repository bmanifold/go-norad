package norad

import (
	"fmt"
)

type ServiceIdentitiesService struct {
	client *Client
}

type ServiceIdentity struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	ServiceId int    `json:"service_id"`
}

type ServiceIdentityOptions struct {
	ServiceIdentityParameters `json:"service_identity"`
}

type ServiceIdentityParameters struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (s ServiceIdentity) String() string {
	return Stringify(s)
}

func (s *ServiceIdentitiesService) GetServiceIdentity(id interface{}) (*ServiceIdentity, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("service_identities/%s", sid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *ServiceIdentity
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *ServiceIdentitiesService) CreateServiceIdentity(sid interface{}, opt *ServiceIdentityOptions) (*ServiceIdentity, *Response, error) {
	service_id, err := parseID(sid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s/service_identities", service_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(ServiceIdentity)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *ServiceIdentitiesService) UpdateServiceIdentity(id interface{}, opt *ServiceIdentityOptions) (*ServiceIdentity, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("service_identities/%s", sid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(ServiceIdentity)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *ServiceIdentitiesService) DeleteServiceIdentity(id interface{}) (*Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("service_identities/%s", sid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
