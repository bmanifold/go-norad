package norad

import (
	"fmt"
)

type OrganizationConfigurationsService struct {
	client NoradClient
}

type OrganizationConfiguration struct {
	Id                      int  `json:"id"`
	OrganizationId          int  `json:"organization_id"`
	AutoApproveDockerRelays bool `json:"auto_approve_docker_relays"`
	UseRelaySshKey          bool `json:"use_relay_ssh_key"`
}

type OrganizationConfigurationOptions struct {
	Parameters OrganizationConfigurationParameters `json:"organization_configuration"`
}

type OrganizationConfigurationParameters struct {
	AutoApproveDockerRelays bool `json:"auto_approve_docker_relays"`
	UseRelaySshKey          bool `json:"use_relay_ssh_key"`
}

func (o OrganizationConfiguration) String() string {
	return Stringify(o)
}

func (service *OrganizationConfigurationsService) Get(org_id interface{}) (*OrganizationConfiguration, *Response, error) {
	oid, err := parseID(org_id)
	if err != nil {
		return nil, nil, err
	}

	// We have to look up the associated org and pluck its config
	u := fmt.Sprintf("organizations/%s", oid)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	o := new(Organization)
	resp, err := service.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o.Configuration, resp, err
}

func (service *OrganizationConfigurationsService) Update(id interface{}, opt *OrganizationConfigurationOptions) (*OrganizationConfiguration, *Response, error) {
	oid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organization_configurations/%s", oid)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(OrganizationConfiguration)
	resp, err := service.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}
