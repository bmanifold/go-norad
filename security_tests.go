package norad

import (
	"fmt"
)

type SecurityTestsService struct {
	client NoradClient
}

type SecurityTest struct {
	Id                       int               `json:"id"`
	Name                     string            `json:"name"`
	Category                 string            `json:"category"`
	ProgArgs                 string            `json:"prog_args"`
	DefaultConfig            map[string]string `json:"default_config"`
	MultiHost                bool              `json:"multi_host"`
	TestTypes                []string          `json:"test_types"`
	Configurable             bool              `json:"configurable"`
	HelpUrl                  string            `json:"help_url"`
	SecurityTestRepositoryId int               `json:"security_test_repository_id"`
}

type SecurityTestOptions struct {
	Parameters SecurityTestParameters `json:"security_container"`
}

type SecurityTestParameters struct {
	Id                       int               `json:"id"`
	Name                     string            `json:"name"`
	Category                 string            `json:"category"`
	ProgArgs                 string            `json:"prog_args"`
	DefaultConfig            map[string]string `json:"default_config"`
	MultiHost                bool              `json:"multi_host"`
	TestTypes                []string          `json:"test_types"`
	Configurable             bool              `json:"configurable"`
	HelpUrl                  string            `json:"help_url"`
	Service                  map[string]string `json:"service"`
	SecurityTestRepositoryId int               `json:"security_test_repository_id"`
}

type SecurityTestFilters struct {
	TestName string `json:"test_name"`
}

func (obj SecurityTest) String() string {
	return Stringify(obj)
}

func (service *SecurityTestsService) List(rid interface{}, filters *SecurityTestFilters) ([]*SecurityTest, *Response, error) {
	repo_id, err := parseID(rid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_test_repositories/%s/security_containers", repo_id)
	req, err := service.client.NewRequest("GET", u, filters)

	if err != nil {
		return nil, nil, err
	}

	security_tests := make([]*SecurityTest, 0)
	resp, err := service.client.Do(req, &security_tests)
	if err != nil {
		return nil, resp, err
	}

	return security_tests, resp, err
}

func (service *SecurityTestsService) Get(id interface{}) (*SecurityTest, *Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_containers/%s", mid)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	security_test := new(SecurityTest)
	resp, err := service.client.Do(req, security_test)
	if err != nil {
		return nil, resp, err
	}

	return security_test, resp, err
}

func (service *SecurityTestsService) Create(rid interface{}, opt *SecurityTestOptions) (*SecurityTest, *Response, error) {
	repo_id, err := parseID(rid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_test_repositories/%s/security_containers", repo_id)
	req, err := service.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	security_test := new(SecurityTest)
	resp, err := service.client.Do(req, security_test)
	if err != nil {
		return nil, resp, err
	}

	return security_test, resp, err
}

func (service *SecurityTestsService) Update(id interface{}, opt *SecurityTestOptions) (*SecurityTest, *Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_containers/%s", mid)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	security_test := new(SecurityTest)
	resp, err := service.client.Do(req, security_test)
	if err != nil {
		return nil, resp, err
	}

	return security_test, resp, err
}

func (service *SecurityTestsService) Delete(id interface{}) (*Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("security_containers/%s", mid)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}
