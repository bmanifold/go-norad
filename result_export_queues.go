package norad

import (
	"fmt"
)

type ResultExportQueuesService struct {
	client NoradClient
}

type ResultExportQueue struct {
	Id             int
	OrganizationId int `json:"organization_id"`
	Type           string
	CreatedBy      string `json:"created_by"`
	AutoSync       bool   `json:"auto_sync"`
}

func (obj ResultExportQueue) String() string {
	return Stringify(obj)
}

func (service *ResultExportQueuesService) List(oid interface{}) ([]*ResultExportQueue, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/result_export_queues", org_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	result_export_queues := make([]*ResultExportQueue, 0)
	resp, err := service.client.Do(req, &result_export_queues)
	if err != nil {
		return nil, resp, err
	}

	return result_export_queues, resp, err
}
