package norad

import (
	"fmt"
)

type SecurityTestConfigsService struct {
	client *Client
}

type SecurityTestConfig struct {
	Id                          int                    `json:"id"`
	Values                      map[string]interface{} `json:"values"`
	Args                        []string               `json:"test_types"`
	EnabledOutsideOfRequirement bool                   `json:"enabled_outside_of_requirement"`
	MachineId                   int                    `json:"machine_id"`
	OrganizationId              int                    `json:"organization_id"`
	SecurityTestId              int                    `json:"security_container_id"`
}

type CreateSecurityTestConfigOptions struct {
	SecurityTestConfigCreationParameters `json:"security_container_config"`
}

type UpdateSecurityTestConfigOptions struct {
	SecurityTestConfigUpdateParameters `json:"security_container_config"`
}

type SecurityTestConfigCreationParameters struct {
	Values                      map[string]interface{} `json:"values"`
	EnabledOutsideOfRequirement bool                   `json:"enabled_outside_of_requirement"`
	SecurityTestId              int                    `json:"security_container_id"`
}

type SecurityTestConfigUpdateParameters struct {
	Values                      map[string]interface{} `json:"values"`
	EnabledOutsideOfRequirement bool                   `json:"enabled_outside_of_requirement"`
}

func (m SecurityTestConfig) String() string {
	return Stringify(m)
}

// Organization-specific endpoints

func (s *SecurityTestConfigsService) ListOrganizationSecurityTestConfigs(oid interface{}) ([]*SecurityTestConfig, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/security_container_configs", org_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*SecurityTestConfig
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestConfigsService) CreateOrganizationSecurityTestConfig(oid interface{}, opt *CreateSecurityTestConfigOptions) (*SecurityTestConfig, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/security_container_configs", org_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SecurityTestConfig)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

// Machine-specific endpoints

func (s *SecurityTestConfigsService) ListMachineSecurityTestConfigs(mid interface{}) ([]*SecurityTestConfig, *Response, error) {
	mach_id, err := parseID(mid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("machines/%s/security_container_configs", mach_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*SecurityTestConfig
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestConfigsService) CreateMachineSecurityTestConfig(mid interface{}, opt *CreateSecurityTestConfigOptions) (*SecurityTestConfig, *Response, error) {
	mach_id, err := parseID(mid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("machines/%s/security_container_configs", mach_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SecurityTestConfig)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestConfigsService) GetSecurityTestConfig(id interface{}) (*SecurityTestConfig, *Response, error) {
	sc_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_container_configs/%s", sc_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *SecurityTestConfig
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestConfigsService) UpdateSecurityTestConfig(id interface{}, opt *UpdateSecurityTestConfigOptions) (*SecurityTestConfig, *Response, error) {
	sc_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_container_configs/%s", sc_id)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SecurityTestConfig)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestConfigsService) DeleteSecurityTestConfig(id interface{}) (*Response, error) {
	sc_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("security_container_configs/%s", sc_id)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
