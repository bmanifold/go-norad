package norad

import (
	"fmt"
)

type SshKeyPairAssignmentsService struct {
	client *Client
}

type SshKeyPairAssignment struct {
	Id           int `json:"id"`
	MachineId    int `json:"machine_id"`
	SshKeyPairId int `json:"ssh_key_pair_id"`
}

type CreateSshKeyPairAssignmentOptions struct {
	SshKeyPairId int `json:"ssh_key_pair_id"`
}

func (kpa SshKeyPairAssignment) String() string {
	return Stringify(kpa)
}

func (s *SshKeyPairAssignmentsService) CreateSshKeyPairAssignment(mid interface{}, opt *CreateSshKeyPairAssignmentOptions) (*SshKeyPairAssignment, *Response, error) {
	machine_id, err := parseID(mid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("machines/%s/ssh_key_pair_assignment", machine_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SshKeyPairAssignment)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshKeyPairAssignmentsService) DeleteSshKeyPairAssignment(mid interface{}) (*Response, error) {
	machine_id, err := parseID(mid)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("machines/%s/ssh_key_pair_assignment", machine_id)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
