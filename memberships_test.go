package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestMembershipServiceParseError(t *testing.T) {
	service := &MembershipsService{}
	_, _, err := service.List(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &MembershipsService{}
	_, _, err = service.Create(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &MembershipsService{}
	_, err = service.Delete(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestMembershipsServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing Memberships with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"organizations/1/memberships",
				nil,
			).Return(&http.Request{}, c.requestError)
			memberships := make([]*Membership, 0)
			client.On(
				"Do",
				&http.Request{},
				&memberships,
			).Return(&Response{}, c.doError)

			service := &MembershipsService{client: &client}
			_, _, err := service.List(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestMembershipsServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a Membership with expected error %s", c.expectedError), func(t *testing.T) {
			opts := MembershipOptions{Parameters: MembershipParameters{Admin: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"organizations/1/memberships",
				&opts,
			).Return(&http.Request{}, c.requestError)
			membership := new(Membership)
			client.On(
				"Do",
				&http.Request{},
				membership,
			).Return(&Response{}, c.doError)

			service := &MembershipsService{client: &client}
			_, _, err := service.Create(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestMembershipsServiceDelete(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a Membership with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"DELETE",
				"memberships/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			client.On(
				"Do",
				&http.Request{},
				nil,
			).Return(&Response{}, c.doError)

			service := &MembershipsService{client: &client}
			_, err := service.Delete(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
