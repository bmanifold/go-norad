package norad

import (
	"fmt"
)

type RequirementGroupsService struct {
	client NoradClient
}

type RequirementGroup struct {
	Id          int
	Name        string
	Description string
}

type RequirementGroupOptions struct {
	Parameters RequirementGroupParameters `json:"requirement_group"`
}

type RequirementGroupParameters struct {
	Name        string
	Description string
}

func (obj RequirementGroup) String() string {
	return Stringify(obj)
}

func (service *RequirementGroupsService) List() ([]*RequirementGroup, *Response, error) {
	req, err := service.client.NewRequest("GET", "requirement_groups", nil)

	if err != nil {
		return nil, nil, err
	}

	requirement_groups := make([]*RequirementGroup, 0)
	resp, err := service.client.Do(req, &requirement_groups)
	if err != nil {
		return nil, resp, err
	}

	return requirement_groups, resp, err
}

func (service *RequirementGroupsService) Get(id interface{}) (*RequirementGroup, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	// We have to look up the associated org and pluck its config
	u := fmt.Sprintf("requirement_groups/%s", channel_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	requirement_group := new(RequirementGroup)
	resp, err := service.client.Do(req, requirement_group)
	if err != nil {
		return nil, resp, err
	}

	return requirement_group, resp, err
}

func (service *RequirementGroupsService) Update(id interface{}, opt *RequirementGroupOptions) (*RequirementGroup, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("requirement_groups/%s", channel_id)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	requirement_group := new(RequirementGroup)
	resp, err := service.client.Do(req, requirement_group)
	if err != nil {
		return nil, resp, err
	}

	return requirement_group, resp, err
}

func (service *RequirementGroupsService) Create(opt *RequirementGroupOptions) (*RequirementGroup, *Response, error) {
	req, err := service.client.NewRequest("POST", "requirement_groups", opt)
	if err != nil {
		return nil, nil, err
	}

	requirement_group := new(RequirementGroup)
	resp, err := service.client.Do(req, requirement_group)
	if err != nil {
		return nil, resp, err
	}

	return requirement_group, resp, err
}
