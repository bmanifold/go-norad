package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestInfosecExportQueueServiceParseError(t *testing.T) {
	service := &InfosecExportQueuesService{}
	_, _, err := service.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &InfosecExportQueuesService{}
	_, _, err = service.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &InfosecExportQueuesService{}
	_, _, err = service.Create(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &InfosecExportQueuesService{}
	_, err = service.Delete(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestInfosecExportQueuesServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a InfosecExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			opts := InfosecExportQueueUpdateOptions{Parameters: InfosecExportQueueUpdateParameters{AutoSync: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"infosec_export_queues/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			infosec_export_queue := new(InfosecExportQueue)
			client.On(
				"Do",
				&http.Request{},
				infosec_export_queue,
			).Return(&Response{}, c.doError)

			service := &InfosecExportQueuesService{client: &client}
			_, _, err := service.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestInfosecExportQueuesServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a InfosecExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			opts := InfosecExportQueueCreateOptions{Parameters: InfosecExportQueueCreateParameters{AutoSync: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"organizations/1/infosec_export_queues",
				&opts,
			).Return(&http.Request{}, c.requestError)
			infosec_export_queue := new(InfosecExportQueue)
			client.On(
				"Do",
				&http.Request{},
				infosec_export_queue,
			).Return(&Response{}, c.doError)

			service := &InfosecExportQueuesService{client: &client}
			_, _, err := service.Create(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestInfosecExportQueuesServiceGet(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a InfosecExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"infosec_export_queues/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			infosec_export_queue_obj := new(InfosecExportQueue)
			client.On(
				"Do",
				&http.Request{},
				infosec_export_queue_obj,
			).Return(&Response{}, c.doError)

			service := &InfosecExportQueuesService{client: &client}
			_, _, err := service.Get(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestInfosecExportQueuesServiceDelete(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a InfosecExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"DELETE",
				"infosec_export_queues/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			client.On(
				"Do",
				&http.Request{},
				nil,
			).Return(&Response{}, c.doError)

			service := &InfosecExportQueuesService{client: &client}
			_, err := service.Delete(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
