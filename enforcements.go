package norad

import (
	"fmt"
)

type EnforcementsService struct {
	client NoradClient
}

type Enforcement struct {
	Id                   int
	OrganizationId       int `json:"organization_id"`
	RequirementGroupId   int `json:"requirement_group_id"`
	RequirementGroupName string
}

type EnforcementOptions struct {
	Parameters EnforcementParameters `json:"enforcement"`
}

type EnforcementParameters struct {
	RequirementGroupId int
}

func (obj Enforcement) String() string {
	return Stringify(obj)
}

func (service *EnforcementsService) List(oid interface{}) ([]*Enforcement, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/enforcements", org_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	enforcements := make([]*Enforcement, 0)
	resp, err := service.client.Do(req, &enforcements)
	if err != nil {
		return nil, resp, err
	}

	return enforcements, resp, err
}

func (service *EnforcementsService) Create(oid interface{}, opt *EnforcementOptions) (*Enforcement, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/enforcements", org_id)
	req, err := service.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	enforcement := new(Enforcement)
	resp, err := service.client.Do(req, enforcement)
	if err != nil {
		return nil, resp, err
	}

	return enforcement, resp, err
}

func (service *EnforcementsService) Delete(id interface{}) (*Response, error) {
	enforcement_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("enforcements/%s", enforcement_id)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}
