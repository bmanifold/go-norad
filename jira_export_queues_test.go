package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestJiraExportQueueServiceParseError(t *testing.T) {
	service := &JiraExportQueuesService{}
	_, _, err := service.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &JiraExportQueuesService{}
	_, _, err = service.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &JiraExportQueuesService{}
	_, _, err = service.Create(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &JiraExportQueuesService{}
	_, err = service.Delete(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestJiraExportQueuesServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a JiraExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			opts := JiraExportQueueUpdateOptions{Parameters: JiraExportQueueUpdateParameters{AutoSync: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"jira_export_queues/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			notification_channel := new(JiraExportQueue)
			client.On(
				"Do",
				&http.Request{},
				notification_channel,
			).Return(&Response{}, c.doError)

			service := &JiraExportQueuesService{client: &client}
			_, _, err := service.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestJiraExportQueuesServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a JiraExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			opts := JiraExportQueueCreateOptions{Parameters: JiraExportQueueCreateParameters{AutoSync: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"organizations/1/jira_export_queues",
				&opts,
			).Return(&http.Request{}, c.requestError)
			notification_channel := new(JiraExportQueue)
			client.On(
				"Do",
				&http.Request{},
				notification_channel,
			).Return(&Response{}, c.doError)

			service := &JiraExportQueuesService{client: &client}
			_, _, err := service.Create(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestJiraExportQueuesServiceGet(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a JiraExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"jira_export_queues/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			notification_channel_obj := new(JiraExportQueue)
			client.On(
				"Do",
				&http.Request{},
				notification_channel_obj,
			).Return(&Response{}, c.doError)

			service := &JiraExportQueuesService{client: &client}
			_, _, err := service.Get(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestJiraExportQueuesServiceDelete(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a JiraExportQueue with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"DELETE",
				"jira_export_queues/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			client.On(
				"Do",
				&http.Request{},
				nil,
			).Return(&Response{}, c.doError)

			service := &JiraExportQueuesService{client: &client}
			_, err := service.Delete(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
