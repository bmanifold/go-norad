package norad

import (
	"fmt"
)

type SecurityTestRepositoriesService struct {
	client *Client
}

type SecurityTestRepository struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Public   bool   `json:"public"`
	Official bool   `json:"official"`
	Host     string `json:"host"`
	Username string `json:"username"`
}

type CreateSecurityTestRepositoryOptions struct {
	SecurityTestRepositoryParameters `json:"security_test_repository"`
}

type UpdateSecurityTestRepositoryOptions struct {
	SecurityTestRepositoryParameters `json:"security_test_repository"`
}

type SecurityTestRepositoryParameters struct {
	Name     string `json:"name"`
	Public   bool   `json:"public"`
	Official bool   `json:"official"`
	Host     string `json:"host"`
	Password string `json:"password"`
	Username string `json:"username"`
}

func (r SecurityTestRepository) String() string {
	return Stringify(r)
}

func (s *SecurityTestRepositoriesService) ListSecurityTestRepositories() ([]*SecurityTestRepository, *Response, error) {
	req, err := s.client.NewRequest("GET", "security_test_repositories", nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*SecurityTestRepository
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestRepositoriesService) GetSecurityTestRepository(id interface{}) (*SecurityTestRepository, *Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_test_repositories/%s", rid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *SecurityTestRepository
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestRepositoriesService) CreateSecurityTestRepository(opt *CreateSecurityTestRepositoryOptions) (*SecurityTestRepository, *Response, error) {
	req, err := s.client.NewRequest("POST", "security_test_repositories", opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SecurityTestRepository)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestRepositoriesService) UpdateSecurityTestRepository(id interface{}, opt *UpdateSecurityTestRepositoryOptions) (*SecurityTestRepository, *Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("security_test_repositories/%s", rid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SecurityTestRepository)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SecurityTestRepositoriesService) DeleteSecurityTestRepository(id interface{}) (*Response, error) {
	rid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("security_test_repositories/%s", rid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
